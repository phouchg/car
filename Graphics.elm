module Graphics exposing (area, blank, print, rect, screen, square)

import Html exposing (..)
import Html.Attributes exposing (..)


area : Int
area =
    400


blank : Html msg
blank =
    text ""


print : String -> String -> Html msg
print colour txt =
    p
        [ style
            [ ( "color", colour )
            , ( "position", "absolute" )
            , ( "top", size 300 )
            ]
        ]
        [ text txt ]


rect : ( Int, Int ) -> ( Int, Int ) -> String -> Html msg
rect ( w, h ) ( x, y ) colour =
    div
        [ style
            [ ( "backgroundColor", colour )
            , ( "height", size h )
            , ( "position", "absolute" )
            , ( "left", size x )
            , ( "top", size y )
            , ( "width", size w )
            ]
        ]
        []


size : Int -> String
size n =
    (toString n) ++ "px"


screen : List (Html msg) -> Html msg
screen children =
    div
        [ style
            [ ( "border", "solid black 1px" )
            , ( "height", size area )
            , ( "margin", "10px 0 0 10px" )
            , ( "position", "relative" )
            , ( "width", size area )
            ]
        ]
        children


square : Int -> ( Int, Int ) -> String -> Html msg
square size =
    rect ( size, size )
