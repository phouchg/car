module Main exposing (..)

import AnimationFrame
import Html exposing (Html)
import Html.App as Html
import Keyboard
import Graphics exposing (blank, print, rect, screen, square)
import Time exposing (Time)


main : Program Never
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


type alias Model =
    { pressed : Bool
    , x : Float
    , light : Int
    , elapsed : Float
    , running : Bool
    , time : Float
    , sun : Float
    , sunDir : Float
    }


init : ( Model, Cmd Msg )
init =
    ( Model False 0 0 0.0 True 0 (toFloat (234 - sunSize)) -1
    , Cmd.none
    )


check : Model -> (Model -> Bool) -> String -> String
check model fn colour =
    if fn model then
        colour
    else
        "lightGray"


isGreen : Model -> Bool
isGreen model =
    model.light == 2


isOrange : Model -> Bool
isOrange model =
    model.light == 1 || model.light == 3


isRed : Model -> Bool
isRed model =
    model.light == 0 || model.light == 1


lightSpeed : Int -> Float
lightSpeed light =
    case light of
        0 ->
            3000

        1 ->
            1000

        2 ->
            4000

        3 ->
            2000

        _ ->
            5000


reactToSpace : Bool -> Model -> Int -> Model
reactToSpace set model key =
    if key == 32 then
        { model | pressed = set }
    else
        model


envColour : Float -> ( String, String )
envColour sun =
    let
        y =
            ceiling sun

        horizon =
            234
    in
        if y > horizon then
            ( "indigo", "saddleBrown" )
        else if y > (horizon - sunSize) then
            ( "darkOrange", "seaGreen" )
        else
            ( "lightBlue", "lightGreen" )


squareSize : Int
squareSize =
    50


squareSpeed : Float
squareSpeed =
    0.05


sunSize : Int
sunSize =
    50


sunSpeed : Float
sunSpeed =
    0.005


sunStart : Float
sunStart =
    25



-- UPDATE


type Msg
    = Down Keyboard.KeyCode
    | Tick Time
    | Up Keyboard.KeyCode


update : Msg -> Model -> ( Model, Cmd Msg )
update action model =
    ( updateModel action model, Cmd.none )


updateEnv : Model -> Float -> Model
updateEnv model diff =
    let
        ls =
            lightSpeed model.light

        elapsed =
            model.elapsed + diff

        ( light, rem ) =
            if elapsed > ls then
                let
                    light' =
                        if model.light == 3 then
                            0
                        else
                            model.light + 1
                in
                    ( light', elapsed - ls )
            else
                ( model.light, elapsed )

        sunDir =
            if model.sun < sunStart then
                1
            else if (ceiling model.sun) >= (Graphics.area - sunSize) then
                -1
            else
                model.sunDir
    in
        { model
            | light = light
            , elapsed = rem
            , sun = model.sun + (sunSpeed * diff * model.sunDir)
            , sunDir = sunDir
            , time = model.time + diff
        }


updateModel : Msg -> Model -> Model
updateModel action model =
    case action of
        Down key ->
            reactToSpace True model key

        Tick diff ->
            if model.running then
                let
                    model' =
                        updateEnv model diff

                    badMove =
                        model.pressed && isRed model

                    goal =
                        (ceiling model.x) == (Graphics.area - squareSize)
                in
                    if badMove || goal then
                        { model | running = False }
                    else if model.pressed then
                        { model' | x = model.x + (squareSpeed * diff) }
                    else
                        model'
            else
                model

        Up key ->
            reactToSpace False model key



-- VIEW


car : Model -> List (Html msg)
car { x, sun } =
    let
        light =
            if sun > 235 then
                [ square 4 ( (ceiling x) + 49, 216 ) "yellow" ]
            else
                []
    in
        List.append
            [ rect ( 30, 10 ) ( (ceiling x) + 10, 200 ) "blue"
            , rect ( squareSize, 15 ) ( ceiling x, 210 ) "blue"
            , square 10 ( (ceiling x) + 5, 225 ) "black"
            , square 10 ( (ceiling x) + 35, 225 ) "black"
            ]
            light


view : Model -> Html Msg
view model =
    let
        msg =
            if model.running then
                if model.x == 0 then
                    print "blue" "Press SPACE to move..."
                else
                    blank
            else if (ceiling model.x) + squareSize < Graphics.area then
                print "red" "You moved on red. You suck at driving."
            else
                print "green" ("Congratulations. You made it across in " ++ (toString (model.time / 1000)) ++ " seconds!")

        ( sky, ground ) =
            envColour model.sun
    in
        screen
            (List.append
                [ rect ( Graphics.area, 235 ) ( 0, 0 ) sky
                , square sunSize ( 320, ceiling model.sun ) "yellow"
                , rect ( Graphics.area, 3 ) ( 0, 235 ) "gray"
                , rect ( Graphics.area, Graphics.area - 238 ) ( 0, 238 ) ground
                , rect ( 25, 65 ) ( 10, 10 ) "black"
                , square 15 ( 15, 15 ) (check model isRed "red")
                , square 15 ( 15, 35 ) (check model isOrange "orange")
                , square 15 ( 15, 55 ) (check model isGreen "green")
                , msg
                ]
                (car model)
            )



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Keyboard.downs Down
        , Keyboard.ups Up
        , AnimationFrame.diffs Tick
        ]
